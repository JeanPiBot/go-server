package server

import (
	"context"
	"errors"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jeanpibot/server/database"
	"github.com/jeanpibot/server/respository"
)

// Aquì va la configuracdion a la base de datos
type Config struct {
	Port        string
	JWTSecret   string
	DataBaseUrl string
}

// esta es la clase server
type Server interface {
	Config() *Config
}

// Este bvoker es el que va a manejar el servidor
type Broker struct {
	config *Config
	router *mux.Router
}

// recibir funcion y es la forma de recibir metodos
func (b *Broker) Config() *Config {
	return b.config
}

// Ahora vamos a definir el constructor
func NewServer(ctx context.Context, config *Config) (*Broker, error) {
	if config.Port == "" {
		return nil, errors.New("The port is required")
	}
	if config.JWTSecret == "" {
		return nil, errors.New("The JWTSecret is required")
	}
	if config.DataBaseUrl == "" {
		return nil, errors.New("The DataBaseUrl is required")
	}

	broker := &Broker{
		config: config,
		router: mux.NewRouter(),
	}

	return broker, nil
}

// otro metodo para el broker
func (b *Broker) Start(binder func(s Server, r *mux.Router)) {
	b.router = mux.NewRouter()
	binder(b, b.router)
	repo, err := database.NewPostgresRepository(b.config.DataBaseUrl)

	if err != nil {
		log.Fatal(err)
	}

	respository.SetRepository(repo)

	log.Println("Server is running on port", b.config.Port)
	if err := http.ListenAndServe(b.config.Port, b.router); err != nil {
		log.Println("error starting server:", err)
	} else {
		log.Fatalf("server stopped")
	}
}
