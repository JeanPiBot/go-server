module github.com/jeanpibot/server

go 1.19

require (
	github.com/golang-jwt/jwt/v5 v5.0.0-rc.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/lib/pq v1.10.7 // indirect
	golang.org/x/crypto v0.8.0 // indirect
)
