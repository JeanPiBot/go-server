package models

import "github.com/golang-jwt/jwt/v5"

type AppClaims struct {
	UserId int64 `json:"userId"`
	jwt.RegisteredClaims
}
