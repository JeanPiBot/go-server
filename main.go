package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/jeanpibot/server/handlers"
	_ "github.com/jeanpibot/server/middleware"
	"github.com/jeanpibot/server/server"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load(".env")

	//Manejador del error
	if err != nil {
		log.Fatal("Error al cargar el .env")
	}
	PORT := os.Getenv("PORT")
	JWT_SECRET := os.Getenv("JWTSECRET")
	DATABASE_URL := os.Getenv("DATABASEURL")

	//Vamos a crear un servidor nuevo
	s, err := server.NewServer(context.Background(), &server.Config{
		JWTSecret:   JWT_SECRET,
		Port:        PORT,
		DataBaseUrl: DATABASE_URL,
	})

	if err != nil {
		log.Fatal(err)
	}

	s.Start(BinderRoutes)

}

func BinderRoutes(s server.Server, r *mux.Router) {

	r.HandleFunc("/", handlers.HomeHandler(s)).Methods(http.MethodGet)
	r.HandleFunc("/signup", handlers.SignUpHandler(s)).Methods(http.MethodPost)
	r.HandleFunc("/login", handlers.LoginHandler(s)).Methods(http.MethodPost)
	r.HandleFunc("/me", handlers.MeHandler(s)).Methods(http.MethodGet)
}
