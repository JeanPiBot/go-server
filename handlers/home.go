package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/jeanpibot/server/server"
)

// Aquì van los response de endpoint principal
type HomeResponse struct {
	Message string `json:"message"`
	Status  bool   `json:"status"`
}

// Aquì va el hanlder como tal
func HomeHandler(s server.Server) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		// se crea la respuesta que se va a enviar
		json.NewEncoder(w).Encode(HomeResponse{
			Message: "Hello world",
			Status:  true,
		})
	}
}
