/* Creaciòn de base de datos */
CREATE DATABASE blog;
/* Creacion de tabla */
CREATE TABLE users(
    id serial primary key,
    email varchar(255),
    password varchar(255),
    created_at timestamp not null default now()
);

/* Creacion de usuario o role para mejorar la seguridad */
CREATE ROLE admin WITH LOGIN PASSWORD 'admin';

/* Asignando permisos a la tabla a utilizar con el usuario*/
GRANT SELECT ON users TO admin;
GRANT UPDATE ON users TO admin;
GRANT INSERT ON users TO admin;
GRANT DELETE ON users TO admin;

/* Asignando permisos para poder implementar los permisos 
 Nota: es importante realizar este comando con el usuario administrador o el superusuario, de igual manera se puede usar el 
 segundo comando para realizar el objetivo */
GRANT ALL ON SEQUENCE users_id_seq TO admin;
GRANT USAGE, SELECT ON SEQUENCE users_id_seq TO admin;

/* Inserciòn de datos de manera manual desde posgres */
insert into users (email,password)
values('test@hotamil.com','test1');
insert into users (email,password)
values('test2@hotamil.com','test2');
insert into users (email,password)
values('test3@hotamil.com','test3');